<?php

return array(
     'factories' => array(
         'Cms\Api\Auth\Extension\AuthManager'            => 'Cms\Api\Auth\Extension\AuthManagerFactory',
         'Zend\Authentication\AuthenticationService' => 'Cms\Api\Auth\Authentication\AuthServiceFactory'
     ),

     'invokables' => array(
        'oauthClients'       => 'Cms\Api\Auth\Entity\OauthClients',
        'oauthAccessTokens'  => 'Cms\Api\Auth\Entity\OauthAccessTokens',
        'oauthScopes'        => 'Cms\Api\Auth\Entity\OauthScopes',
        'members'            => 'Cms\Api\Auth\Entity\Members'
    
    )
);