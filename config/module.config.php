<?php
use ZfcRbac\Guard\GuardInterface;
use Zend\Mvc\MvcEvent;

return array(

    'service_manager' => include 'service.config.php',

    'doctrine' => array(
        // 'authentication' => array(
        //     'orm_default' => array(
        //         'object_manager' => 'Doctrine\ORM\EntityManager',
        //         'identity_class' => 'Cms\Auth\Entity\Members',
        //         'identity_property' => 'email',
        //         'credential_property' => 'password',
        //         // 'credential_callable' => function(\Cms\Database\Entity\Members $user, $passwordGiven) {
        //         //     $bcrypt = new \Zend\Crypt\Password\Bcrypt();
        //         //     return $bcrypt->verify($passwordGiven, $user->getPassword());
                    
        //         // },
        //     ),
        // ),
        'driver' => array(
            'application_entities' => array(
              'paths' => array(__DIR__ . '/../src/Entity')
            ),
            'orm_default' => array(
              'drivers' => array(
                'Cms\Api\Auth\Entity' => 'application_entities'
              )
            )
        ),

    ),

    // 'zfc_rbac' => array(
    //     'protection_policy' => GuardInterface::POLICY_ALLOW,
    //     'guards' => array(
    //         'ZfcRbac\Guard\ControllerGuard' => [
    //             [
    //                 'controller' => 'cms.controller.admin',
    //                 'action'     => 'dashboard',
    //                 'roles'      => ['member']
    //             ],
    //             [
    //                 'controller' => 'cms.admin.members',
    //                 'actions'    => ['members', 'create'],
    //                 'roles'      => ['superman']
    //             ]
    //         ]

    //     ),
    //     'identity_provider' => 'ZfcRbac\Identity\AuthenticationIdentityProvider',
    //     'role_provider' => [
    //         'ZfcRbac\Role\InMemoryRoleProvider' => [
    //             'guest' => [
    //                 //'permissions' => []
    //             ],
    //             'member' => [
    //                 'children'    => ['guest'],
    //                 //'permissions' => ['admin/dashboard']
    //             ],

    //             'hulk' => [
    //                 'children'    => ['member'],
    //                 //'permissions' => []
    //             ],
    //             'superman' => [
    //                 'children'    => ['hulk'],
    //                 //'permissions' => ['admin/members']
    //             ],            ]   
    //     ],

    //     'redirect_strategy' => [
    //         'redirect_when_connected'        => true,
    //         'redirect_to_route_connected'    => 'cms-admin',
    //         'redirect_to_route_disconnected' => 'cms-login',
    //         'append_previous_uri'            => false,
    //         'previous_uri_query_key'         => 'redirectTo'
    //     ],
    // ),

         
    'bas_cms' => array(
    
        'extensions' => array(
            'auth-manager' => array(
                'type'    => 'Cms\Api\Auth\Extension\AuthManager',
                'options' => array(
                    'listeners' => array(
                        'cms.auth.create.user' => 'createUserEvent'
                    )
                )
            )
        )
    )
);
