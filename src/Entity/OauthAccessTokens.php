<?php
namespace Cms\Api\Auth\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="oauth_access_tokens", uniqueConstraints={@ORM\UniqueConstraint(name="access_token_pk",columns={"access_token"})})
 */
class OauthAccessTokens {
    
    /**
    * @ORM\Id
    * @ORM\Column(name="access_token", type="string", length=40, nullable=false)
    */
    protected $accessToken;

    /** @ORM\Column(name="client_id", type="string", length=80, nullable=false) */
    protected $clientId;

    /** @ORM\Column(name="user_id", type="string", nullable=true) */
    protected $userId;

    /** @ORM\Column(name="expires", type="datetime", nullable=false) */
    protected $expires;

    /** @ORM\Column(name="scope", type="string", length=2000, nullable=true) */
    protected $scope;

    

    public function getAccessToken() {
        return $this->accessToken;
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken; 
    }

    public function getClientId() {
        return $this->clientId;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function getUserId() {
        return $this->userId;
    }


    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function getExpires() {
        return $this->expires;
    }

    public function setExpires($expires) {
        $this->expires = $expires;
    }

    public function getScope() {
        return $this->scope;
    }

    public function setScope($scope) {
        $this->scope = $scope;
    }
}