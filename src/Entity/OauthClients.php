<?php
namespace Cms\Api\Auth\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="oauth_clients", uniqueConstraints={@ORM\UniqueConstraint(name="clients_client_id_pk",columns={"client_id"})})
 */
class OauthClients {
    
    public function __construct() {
        $this->redirectUri = '/oauth/recievecode';
    }

    /**
    * @ORM\Id
    * @ORM\Column(name="client_id", type="string")
    */
    protected $clientId;

    /** @ORM\Column(name="client_secret", type="string", nullable=false) */
    protected $clientSecret;

    /** @ORM\Column(name="redirect_uri", type="string", nullable=false) */
    protected $redirectUri;

    /** @ORM\Column(name="grant_type", type="string", nullable=true) */
    protected $grantTypes;

    /** @ORM\Column(name="scope", type="string", nullable=true) */
    protected $scope;

    /** @ORM\Column(name="user_id", type="string", nullable=true) */
    protected $userId;

    public function getClientId() {
        return $this->clientId;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function getClientSecret() {
        return $this->clientSecret;
    }

    public function setClientSecret($secret) {
        $this->clientSecret = (string) $secret;
    }

    public function getRedirectUri() {
        return $this->redirectUri;
    }


    public function setRedirectUri($redirectUri) {
        $this->redirectUri = $redirectUri;
    }

    public function getGrantType() {
        return $this->grantType;
    }

    public function setGrantType($grantType) {
        $this->grantType = $grantType;
    }

    public function getScope() {
        return $this->scope;
    }

    public function setScope($scope) {
        $this->scope = $scope;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }
}