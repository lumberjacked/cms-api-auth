<?php
namespace Cms\Api\Auth\Entity;

use Doctrine\ORM\Mapping as ORM;
use ZfcRbac\Identity\IdentityInterface;

/**
 * @ORM\Entity(repositoryClass="Cms\Api\Auth\Repository\MembersRepository")
 */
class Members implements IdentityInterface {

    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

    /** @ORM\Column(type="string") */
    protected $memberSince;

    ///** @ORM\Column(type="string") */
    //protected $invitedBy;

    /** @ORM\Column(type="string") */
    protected $email;

    /** @ORM\Column(type="string", nullable=true) */
    protected $username;

    /** @ORM\Column(type="string") */
    protected $password;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $firstname;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $lastname;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $address;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $city;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $state;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $gmail;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $facebook;

    // /** @ORM\Column(type="string", nullable=true) */
    // protected $twitter;

    /** @ORM\Column(type="string") */
    protected $roles;

    /** @ORM\Column(type="string", nullable=true) */
    protected $accessToken;

    public function __construct() {
        $this->memberSince = date('Y-m-d H:i:s');
    }

    public function getId() {
        return $this->id;
    }

    public function setMemberSince($memberSince) {
        $this->memberSince = $memberSince;
    }

    public function getMemberSince() {
        return $this->memberSince;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    // public function setUsername($username) {
    //     $this->username = $username;
    // }

    // public function getUsername() {
    //     return $this->username;
    // }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getPassword() {
        return $this->password;
    }

    // public function setFirstname($firstname) {
    //     $this->firstname = $firstname;
    // }

    // public function getFirstname() {
    //     return $this->firstname;
    // }

    // public function setLastname($lastname) {
    //     $this->lastname = $lastname;
    // }

    // public function getLastname() {
    //     return $this->lastname;
    // }

    // public function setAddress($address) {
    //     $this->address = $address;
    // }

    // public function getAddress() {
    //     return $this->address;
    // }

    // public function setCity($city) {
    //     $this->city = $city;
    // }

    // public function getCity() {
    //     return $this->city;
    // }

    // public function setState($state) {
    //     $this->state = $state;
    // }

    // public function getState() {
    //     return $this->state;
    // }

    // public function setGmail($gmail) {
    //     $this->gmail = $gmail;
    // }

    // public function getGmail() {
    //     return $this->gmail;
    // }

    // public function setFacebook($facebook) {
    //     $this->facebook = $facebook;
    // }

    // public function getFacebook() {
    //     return $this->facebook;
    // }

    // public function setTwitter($twitter) {
    //     $this->twitter = $twitter;
    // }

    // public function getTwitter() {
    //     return $this->twitter;
    // }

    public function setRoles($role) {
        $this->roles = $role;
    }

    public function getRoles() {
        return $this->roles;
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken;
    }

    public function getAccessToken() {
        return $this->accessToken;
    }

    public function __get($name) {
        return $this->$name;
    }
}
