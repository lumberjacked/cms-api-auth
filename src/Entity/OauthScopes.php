<?php
namespace Cms\Api\Auth\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="oauth_scopes")
 */
class OauthScopes {
    
    /**
    * @ORM\Id 
    * @ORM\Column(name="client_id", type="string", length=80)
    */
    protected $clientId;

    /** @ORM\Column(name="type", type="string", nullable=false) */
    protected $type;

    /** @ORM\Column(name="scope", type="string", length=2000) */
    protected $scope = 'supported';

    /** @ORM\Column(name="is_default", type="smallint") */
    protected $is_default;

    public function getClientId() {
        return $this->clientId;
    }

    public function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    public function getType() {
        return $this->clientId;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getScope() {
        return $this->scope;
    }

    public function setScope($scope) {
        $this->scope = $scope;
    }

    public function getIsDefault() {
        return $this->is_default;
    }

    public function setIsDefault($is_default) {
        $this->is_default = $is_default;
    }
}