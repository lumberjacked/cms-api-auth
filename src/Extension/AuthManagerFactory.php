<?php
namespace Cms\Api\Auth\Extension;

use Zend\ServiceManager\FactoryInterface;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceLocatorInterface;

class AuthManagerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        
        return new AuthManager();
    }
}