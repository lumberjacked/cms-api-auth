<?php
namespace Cms\Api\Auth\Extension;

use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;
use Zend\Crypt\Password\Bcrypt;
use Cms\Auth\Authentication\AuthAdapter;
use Zend\Authentication\AuthenticationService;
use Cms\ExtensionManager\Extension\ResponderEvent;
use Cms\ExtensionManager\Extension\AbstractExtension;
use Zend\Authentication\Result as AuthenticationResult;

class AuthManager extends AbstractExtension {

    protected $identity;

    protected $credentials;
    
    public function __construct() {
        $this->identifer = get_called_class();
    }

    public function createUserEvent(ResponderEvent $e) {

        $params = $e->getParams();
        
        if(empty($params) || !array_key_exists('email', $params) || !array_key_exists('password', $params) || !array_key_exists('password_confirm', $params)) {
            return $e->responder(true, 'Are you expecting an imaginery user?', null, 401);
        }

        if(!array_key_exists('roles', $params)) {
            $params['roles'] = 'memeber';
        }
        
        if($params['password'] === $params['password_confirm']) {
            $params['password'] = $this->hash($params['password']);
            unset($params['password_confirm']);
        }
        
        $em       = $this->get('dbmanager')->getEntityManager();
        $hydrator = $this->get('hydrator');
        
        $em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            
            $user  = $this->get('members');
            $oauth = $this->get('oauthClients');
            
            $user  = $hydrator->hydrate($params, $user);
            $em->persist($user);
            
            $oauth->setClientId($user->getEmail());
            $oauth->setClientSecret($user->getPassword());
            $em->persist($oauth);

            $em->flush();
            $em->getConnection()->commit();
        
            return $e->responder(null, false, sprintf('created user %s', $params['email']), $params);

        } catch (DBALException $e) {
            
            $em->getConnection()->rollback();
            $em->close();
            
            return $e->responder(null, true, $e->getMessage(), $params, 500);
        }
    }

    protected function hash($password) {

        $crypter = new Bcrypt();
        return $crypter->create($password);
    }

    protected function verify($password, $securePassword) {
        
        $bcrypt = new Bcrypt();
        return $bcrypt->verify($password, $securePassword);
    }



}