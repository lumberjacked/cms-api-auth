<?php
namespace Cms\Api\Auth;

use Zend\Stdlib;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\ModuleRouteListener;
use Zend\EventManager\Event;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements
    ConfigProviderInterface,
    ServiceProviderInterface
{

    //TODO:: This might can be removed because there isnt any redirects now after oauth
    // public function onBootstrap(MvcEvent $e) {
    //     $eventManager = $e->getApplication()->getEventManager();
        
    //     $moduleRouteListener = new ModuleRouteListener();
    //     $moduleRouteListener->attach($eventManager);

    //     $serviceManager = $e->getApplication()->getServiceManager();

    //     $eventManager->attach(
    //         $serviceManager->get('ZfcRbac\View\Strategy\RedirectStrategy')
    //     );
    // }
    
    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return include __DIR__ . '/config/service.config.php';
    }
}